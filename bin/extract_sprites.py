import cssutils
import re

def get_image_from_id(id):
    if id <= 346:
        return 'rebirth'
    elif id <= 441:
        return 'afterbirth'
    elif id <= 552:
        return 'afterbirth-plus'
    else:
        return 'repentance'

with open('sprites.css', 'r') as f:
    sheet = cssutils.parseString(f.read(), validate=False)

style = list()
for rule in sheet.cssRules:
    try:
        if  rule.type == cssutils.css.CSSRule.STYLE_RULE and re.match('.*((re|abn|apn)-itm|rep)\d\d\d', rule.selectorText):
            id = rule.selectorText[-3:]
            rule.selectorText = ".item-" + id
            offset = rule.style.removeProperty('background-position')
            if rule.style.getProperty('width') == None:
                rule.style.setProperty('width', '50px')
            rule.style.setProperty('background', f"url('./images/{get_image_from_id(int(id))}-items.png') {offset}")
            style.append(rule.cssText)
    except Exception as e:
        print(e)
print('\n'.join(style))
