from bs4 import BeautifulSoup, NavigableString
import json
import urllib.request

def next_sibs(cursor):
    sibs = []
    for next in cursor.next_siblings:
        if (isinstance(next, NavigableString) == False):
            sibs.append(next)
    return sibs

def parseRecipesPage(url):
    with urllib.request.urlopen(url) as f:
        soup = BeautifulSoup(f.read())
    items = dict()
    for cur in soup.find_all('span', attrs={'class': 'tooltip'}):
        cur = cur.find_next('td')
        id = cur.text.strip()
        items[id] = []
        for recipe in cur.next_siblings:
            if (isinstance(recipe, NavigableString) == False):
                items[id].append([r['alt'].split('(')[-1].split(')')[0] for r in recipe.find_all('img')])
    return items


# recipes_data = {
#     **parseRecipesPage('https://bindingofisaacrebirth.fandom.com/wiki/Bag_of_Crafting_(Recipes)_-_Active'),
#     **parseRecipesPage('https://bindingofisaacrebirth.fandom.com/wiki/Bag_of_Crafting_(Recipes)_-_Passive_(1-399)'),
#     **parseRecipesPage('https://bindingofisaacrebirth.fandom.com/wiki/Bag_of_Crafting_(Recipes)_-_Passive_(400-727)')
# }
#
# with open('recipes.json', 'w') as f:
#     f.write(json.dumps(recipes_data, sort_keys=True, indent=4))

with open('items.dump', 'r') as f:
    soup = BeautifulSoup(f.read())

items_data = dict()
trinkets_data = dict()
consumables_data = dict()

for raw in soup.find_all('p', attrs={'class': 'item-title'}):
    raw = raw.parent
    item = dict()
    consumables_id = 0
    cat = 0
    for field in raw.find_all('p'):
        try:
            cls = field['class'][0]
            if (cls == 'item-title'):
                item['name'] = field.get_text()
            elif (cls == 'pickup'):
                item['text'] = field.get_text().strip('""')
            elif (cls == 'r-itemid'):
                id = field.get_text()
                if (id.startswith('TrinketId:')):
                    cat = 1
                elif (id.startswith('ItemID:')):
                    cat = 2
                item['id'] = id.split(':')[1].strip()
            elif (cls == 'quality'):
                item['quality'] = field.get_text().split(':')[-1].strip()
        except KeyError:
            data = field.get_text().split(':')
            if (data[0] == 'Type'):
                item['type'] = [s.strip() for s in data[1].split(',')]
            elif (data[0] == 'Recharge time'):
                item['cooldown'] = data[1].strip().split(' ')[0]
            elif (data[0] == 'Item Pool'):
                item['pool'] = [s.strip() for s in data[1].split(',')]
        except Exception as e:
            print(e)
    if (cat == 0):
        consumables_data[str(consumables_id)] = item
        consumables_id += 1
    elif (cat == 1):
        trinkets_data[item['id']] = item
    else:
        items_data[item['id']] = item

with open('pedestals.json', 'w') as f:
    f.write(json.dumps(items_data, sort_keys=True, indent=4))

with open('trinkets.json', 'w') as f:
    f.write(json.dumps(trinkets_data, sort_keys=True, indent=4))

with open('consumables.json', 'w') as f:
    f.write(json.dumps(consumables_data, sort_keys=True, indent=4))
