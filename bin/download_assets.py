from bs4 import BeautifulSoup, NavigableString
import json
import urllib.request

def next_sibs(cursor):
    sibs = []
    for next in cursor.next_siblings:
        if (isinstance(next, NavigableString) == False):
            sibs.append(next)
    return sibs

def dl_assets(dir, url, id, prefix):
    with urllib.request.urlopen(url) as f:
        soup = BeautifulSoup(f.read())
    for cur in soup.select(id):
        cols = next_sibs(cur.td)
        id = cols[0].text.strip().split('.')[-1]
        img = cols[1].a.img.attrs['src']
        urllib.request.urlretrieve(img, f"{dir}/{prefix}_{id}.png")

dl_assets('assets/items', 'https://bindingofisaacrebirth.fandom.com/wiki/Items', '.row-collectible', 'item')
dl_assets('assets/trinkets', 'https://bindingofisaacrebirth.fandom.com/wiki/Trinkets', '.row-trinket', 'trinket')