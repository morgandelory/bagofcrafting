import json

stats = ['tears', 'tears_x', 'dmg', 'dmg_x', 'height', 'range', 'shot_speed', 'speed', 'luck', 'soul_hearts', 'sin_hearts', 'health']
transformations = ['conjoined', 'beelzebub', 'funguy', 'spun', 'yesmother', 'seraphim', 'bookworm', 'ohcrap', 'bob', 'leviathan', 'guppy', 'spiderbaby', 'superbum']

def mergedicts(dict1, dict2):
    for k in set(dict1.keys()).union(dict2.keys()):
        if k in dict1 and k in dict2:
            if isinstance(dict1[k], dict) and isinstance(dict2[k], dict):
                yield (k, dict(mergedicts(dict1[k], dict2[k])))
            else:
                # If one of the values is not a dict, you can't continue merging it.
                # Value from second dict overrides one in first and we move on.
                yield (k, dict2[k])
                # Alternatively, replace this with exception raiser to alert you of value conflicts
        elif k in dict1:
            yield (k, dict1[k])
        else:
            yield (k, dict2[k])

with open('items.json') as json_file:
    data = json.load(json_file)
    db = dict()
    del data['NEW']
    del data['-1']
    for k, v in data.items():
        if int(k) <= 729:
            db[k] = dict()
            if 'text' in data[k]:
                db[k]['description'] = data[k]['text']
            for field, value in data[k].items():
                if field in transformations:
                    db[k]['transformation'] = field
                if field in stats:
                    if 'stats' not in db[k]:
                        db[k]['stats'] = dict()
                    db[k]['stats'][field] = value

with open('pedestals.json', 'r') as inp, open('items_data.json', 'w') as out:
    data = json.load(inp)
    data = dict(mergedicts(data, db))

    out.write(json.dumps(data, sort_keys=True, indent=4))


