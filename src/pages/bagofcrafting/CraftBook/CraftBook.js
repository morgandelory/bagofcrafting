import {Grid, ListItem} from "@material-ui/core";
import React from 'react'
import Collectible from "../../../components/Collectible/Collectible"
import CraftingComponent from "../CraftingComponent/CraftingComponent";
import {areEqual, FixedSizeList} from "react-window";
import Pedestals from "../../../data/pedestals.json";
import useWindowDimensions from "../../../hooks/useWindowDimensions";
import {Typo} from "../../../components/Typo/Typo";
import Scrollbar from "../../../components/Scrollbar/Scrollbar";
import AutoSizer from "react-virtualized-auto-sizer";

const SingleRecipe = React.memo((props) => {
    const comp = {...props.components};
    const available = props.recipe.map(c => --comp[c] >= 0)
    const canBuild = available.every(ok => ok === true);

    return <Grid container
                 direction={'row'}
                 justify={'center'}
                 style={{
                     flex: '0 0 256px',
                     border: `${canBuild ? '1' : '0'}px solid black`,
                     cursor: `${canBuild ? 'pointer' : 'initial'}`
                 }}
                 onClick={canBuild ? () => props.addCraft(props.recipe) : () => {
                 }}
    >
        {
            props.recipe.map((component, index) => {
                return <Grid item key={index}>
                    <CraftingComponent craftingid={component} enabled={available[index]}/>
                </Grid>
            })
        }
    </Grid>
})

const filterComponents = (components, recipe) => Object.keys(components)
    .filter(key => recipe.includes(key))
    .reduce((obj, key) => {
        return {
            ...obj,
            [key]: components[key]
        };
    }, {});

const RecipeEntry = React.memo(({data, index, style}) => {
    const item = data.craftBook[index];

    const addCraft = (recipe) => {
        data.addCraft(item.id, recipe)
    };

    return <ListItem style={{...style, borderBottom: '1px solid black'}}>
        <Grid container>
            <Grid item container direction={'row'} alignItems={'center'}>
                <Grid item>
                    <Typo variant={'h5'} style={{color: 'white'}}>{Pedestals[item.id].name}</Typo>
                </Grid>
                <Grid item>
                    <Collectible id={item.id}/>
                </Grid>
            </Grid>
            <Grid item container direction={'row'} justify={'space-evenly'}>
                {
                    item.recipes.map((r, i) => {
                        return <Grid item key={i} xs={12} lg={6}>
                            <SingleRecipe recipe={r}
                                          components={filterComponents(data.components, r)}
                                          addCraft={addCraft}
                            />
                        </Grid>
                    })
                }
            </Grid>
        </Grid>
    </ListItem>
}, areEqual)

const CraftBook = React.forwardRef((props, ref) => {
    const {width} = useWindowDimensions();

    // xs, extra-small: 0px
    // sm, small: 600px
    // md, medium: 960px
    // lg, large: 1280px
    // xl, extra-large: 1920p
    const itemSize = (() => {
        if (width >= 1310)
            return 149
        if (width >= 630)
            return 213
        return 373
    })();

    return <AutoSizer>
        {size => (
            <FixedSizeList height={size.height} width={size.width}
                           itemSize={itemSize}
                           itemCount={props.craftBook.length}
                           overscanCount={2}
                           ref={ref}
                           outerElementType={Scrollbar}
                           itemData={{
                               components: props.components,
                               addCraft: props.addCraft,
                               craftBook: props.craftBook
                           }}
            >{RecipeEntry}</FixedSizeList>
        )}
    </AutoSizer>
})

export default CraftBook;
