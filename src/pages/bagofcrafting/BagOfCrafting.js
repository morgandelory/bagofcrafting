import {Grid} from "@material-ui/core";
import Workbench from "./Workbench/Workbench";
import CraftBook from "./CraftBook/CraftBook";
import Pockets from "./Pockets/Pockets";
import React, {useEffect, useRef, useState} from "react";
import {componentsInit} from "../../data/const";
import Pedestals from "../../data/pedestals.json";
import Recipes from "../../data/recipes.json";

const genCraftBook = () => Object.keys(Recipes).map(k => {
    return {id: k, recipes: Recipes[k]}
});

const BagOfCrafting = () => {
    const [craftBook, setCraftBook] = useState(genCraftBook());
    const [components, setComponents] = useState(componentsInit);
    const [craftList, setCraftList] = useState([]);

    const scrollRef = useRef();

    const sortCraftBook = () => {
        const tmpCraftBook = [...craftBook];

        tmpCraftBook.forEach(item => {
            const tmp_components = {...components};

            item.progress = item.recipes.map(recipe => {
                return recipe.map(c => --tmp_components[c] >= 0 ? 1 : 0).reduce((a, b) => a + b, 0)
            }).reduce((a, b) => a > b ? a : b, 0)
        });
        tmpCraftBook.sort((a, b) => {
            if (a.progress !== b.progress) {
                return b.progress - a.progress;
            }
            else if (Pedestals[b.id].quality !== Pedestals[a.id].quality) {
                return Pedestals[b.id].quality - Pedestals[a.id].quality;
            }
            return parseInt(b.id) - parseInt(a.id);
        })
        setCraftBook(tmpCraftBook);
    };
    useEffect(sortCraftBook, [components]);

    // const scrollToTop = () => scrollRef.current.scrollTo(0);
    // useEffect(scrollToTop, [craftBook]);

    const addCraft = (itemid, recipe) => {
        const tmpComponents = {...components};
        const canBuild = recipe.map(c => --tmpComponents[c] >= 0).reduce((a, k) => a && k);
        if (canBuild === true) {
            setCraftList([...craftList, {itemid: itemid, recipe: recipe}])
            setComponents(tmpComponents);
        }
    }

    const deleteCraft = index => {
        const tmp = [...craftList];
        const trash = tmp.splice(index, 1)[0];
        const tmp_components = {...components};
        trash.recipe.forEach(c => ++tmp_components[c]);
        setCraftList(tmp);
        setComponents(tmp_components);
    }

    return <Grid container direction={'row'} justify={'space-between'}>
        <Grid item xs={3}>
            <Workbench components={components}
                       setComponents={setComponents}
            />
        </Grid>
        <Grid item xs={5}>
            <CraftBook ref={scrollRef} craftBook={craftBook} components={components} addCraft={addCraft}/>
        </Grid>
        <Grid item xs={3}><Pockets craftList={craftList} deleteCraft={deleteCraft}/></Grid>
    </Grid>
}

export default BagOfCrafting;