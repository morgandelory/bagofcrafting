import './CraftingComponent.css'

function CraftingComponent(props) {
    const x = Math.floor(parseInt(props.craftingid) / 8);
    const y = parseInt(props.craftingid) % 8;

    return <div className={'crafting-component'}
                style={{
                    height: '32px',
                    width: '32px',
                    backgroundPosition: `-${y * 32}px -${x * 32}px`,
                    opacity: props.enabled ? '1' : '0.4'
                }}/>
}

export default CraftingComponent;
