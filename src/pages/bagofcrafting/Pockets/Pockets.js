import {Grid} from "@material-ui/core";
import Collectible from "../../../components/Collectible/Collectible";

const Pockets = ({craftList, deleteCraft}) => {
    return <Grid container>
        {
            craftList.map((craft, k) => {
                return <Grid item key={k} onClick={_ => deleteCraft(k)} style={{cursor: 'pointer'}}>
                    <Collectible id={craft.itemid}/>
                </Grid>
            })
        }
    </Grid>
}

export default Pockets;