import {Grid} from "@material-ui/core";
import CraftingComponent from "../CraftingComponent/CraftingComponent";
import {useEffect} from "react";
import {ClickableTypo, Typo} from "../../../components/Typo/Typo";
import PostIt from "../../../components/PostIt/PostIt";
import {componentsInit} from "../../../data/const";

const ComponentButton = (props) => {
    return <div onClick={props.onClick} style={{
        userSelect: 'none',
        backgroundColor: 'white',
        width: '12px',
        height: '12px',
        border: '2px solid black',
        textAlign: 'center',
        lineHeight: '12px',
        borderRadius: '12px',
        cursor: 'pointer'
    }}>{props.children}</div>
}

const ComponentValue = (props) => {
    return <div style={{
        width: '18px',
        height: '12px',
        textAlign: 'center',
        lineHeight: '12px',
    }}>{props.children}</div>
}

function Component(props) {
    const value = props.components[props.craftingid]
    const addComponent = () => props.setComponents({...props.components, [props.craftingid]: value + 1})
    const removeComponent = () => {
        if (value > 0) {
            props.setComponents({...props.components, [props.craftingid]: value - 1})
        }
    }

    return <Grid container direction={'row'} justify={'center'} alignItems={'center'}>
        <Grid item>
            <CraftingComponent craftingid={props.craftingid} enabled/>
        </Grid>
        <Grid item><ComponentButton onClick={removeComponent}>-</ComponentButton></Grid>
        <Grid item><ComponentValue>{props.components[props.craftingid]}</ComponentValue></Grid>
        <Grid item><ComponentButton onClick={addComponent}>+</ComponentButton></Grid>
    </Grid>
}

function Workbench(props) {
    const clearComponents = () => props.setComponents({...componentsInit});

    const saveComponents = () => props.setComponents(props.components);
    useEffect(saveComponents, [props.components]);

    return <PostIt container justify={('center')}>
        <Grid item>
            <Typo variant={'h6'} style={{color: 'black'}}>Components :</Typo>
        </Grid>
        <Grid item container justify={'space-evenly'}>
            {
                [...Array(25).keys()].filter(id => [16, 19, 23, 24].includes(id) === false).map(id =>
                    <Grid item key={id}>
                        <Component craftingid={(id + 1)}
                                   components={props.components}
                                   setComponents={props.setComponents}
                        />
                    </Grid>
                )
            }
        </Grid>
        <Grid item container justify={'space-evenly'}>
            <Grid item><ClickableTypo variant={'h6'} onClick={clearComponents}>Clear</ClickableTypo></Grid>
        </Grid>
    </PostIt>;
}

export default Workbench;