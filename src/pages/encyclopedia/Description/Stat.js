import {Grid} from "@material-ui/core";
import {Typo} from "../../../components/Typo/Typo";
import Health from "./Health/Health";

const StatEntry = props => {
    const neg = (props.value < 0);

    return <Grid item container direction={'row'} xs={12}>
        <Grid item xs><Typo variant={'subtitle2'}>{`${props.name} :`}</Typo></Grid>
        <Grid item xs><Typo variant={'subtitle2'} style={{color: neg ? 'red' : 'green'}}>{(neg ? "" : "+") + props.value}</Typo></Grid>
    </Grid>
}

const Stats = props => {
    return <Grid item container xs={10} direction={'column'} alignItems={'center'}
                 style={{borderTop: 'solid 1px black'}}
    >
        <Health stats={props.stats}/>
        {
            Object.entries(props.stats).map(stat => <StatEntry key={`${props.id}_${stat[0]}`} name={stat[0]} value={stat[1]}/>)
        }
    </Grid>
}

export default Stats;