import Collectible from "../../../components/Collectible/Collectible";
import {Grid} from "@material-ui/core";
import {Typo} from "../../../components/Typo/Typo";
import Pedestals from "../../../data/items_data.json"
import Stats from "./Stat";

const Header = props => {
    return <Grid item container direction={'row'} justify={'space-around'}>
        <Grid item xs={3}>
            <Collectible {...props}/>
        </Grid>
        <Grid item xs={9} container direction={'column'} justify={'center'} alignContent={'center'}>
            <Grid item>
                <Typo variant={'h6'}>{props.name}</Typo>
            </Grid>
            <Grid item>
                <Typo variant={'subtitle2'}>"{props.text}"</Typo>
            </Grid>
        </Grid>
    </Grid>
}

const Description = props => {
    const item = Pedestals[props.id];

    return <Grid container justify={'center'} spacing={3}>
            <Header {...item}/>
            {'stats' in item ? <Stats {...item}/> : null}
            <Grid item xs={10} style={{borderTop: 'solid 1px black'}}>
                <Typo variant={'body2'}>{item.description}</Typo>
            </Grid>
        </Grid>
}

export default Description;