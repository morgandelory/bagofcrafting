import ui_hearts from './images/ui_hearts.png'
import {Grid} from "@material-ui/core";
import {Typo} from "../../../../components/Typo/Typo";

const Heart = props => {
    const offset = {
        health: [0, 0],
        soul_hearts: [0, 1],
        sin_hearts: [2, 1]
    }

    return <div style={{
        height: '32px',
        width: '32px',
        imageRendering: 'pixelated',
        backgroundSize: '700%',
        backgroundImage: `url(${ui_hearts})`,
        backgroundPosition: `-${offset[props.heartType][0] * 32}px -${offset[props.heartType][1] * 32}px`
    }}/>
}

const health_stats = ['health', 'soul_hearts', 'sin_hearts'];

const Health = props => {
    const health = Object.keys(props.stats)
        .filter(k => health_stats.includes(k))
        .reduce((obj, key) => {
            obj[key] = props.stats[key];
            return obj
        }, {});

    return health.length === 0 ? null :
        <Grid item container direction={'row'} xs={12} alignItems={'flex-end'}>
            <Grid item xs><Typo variant={'subtitle2'}>Health :</Typo></Grid>
            <Grid item xs container direction={'row'}>
                {
                    Object.entries(health).map(k => Array(parseInt(k[1])).fill(0).map(
                        (_, i) => <Heart key={`${props.id}_${k[0]}_${i}`} heartType={k[0]}/>
                    ))
                }
            </Grid>
        </Grid>
}

export default Health;
