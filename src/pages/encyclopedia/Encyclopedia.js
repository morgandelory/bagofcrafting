import React, {useState} from "react";
import {Grid} from "@material-ui/core";
import PostIt from "../../components/PostIt/PostIt";
import Description from "./Description/Description";
import Index from "./Index/Index";
import Reminder from "./Reminder/Reminder";

const Encyclopedia = () => {
    const [selectedItem, setSelectedItem] = useState(null);

    return <Grid container direction={'row'} justify={'space-around'}>
        {
            selectedItem === null ? <Grid item xs={3}/> :
            <PostIt item xs={3} justify={'center'}>
                <Description id={selectedItem}/>
            </PostIt>
        }
        <Grid item xs={5}>
            <Index onChange={setSelectedItem}/>
        </Grid>
        <Grid item xs={3}>
            <Reminder />
        </Grid>
    </Grid>
}

export default Encyclopedia;