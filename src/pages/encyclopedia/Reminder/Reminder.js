import PostIt from "../../../components/PostIt/PostIt";
import {useState} from "react";
import {Grid} from "@material-ui/core";
import {Typo} from "../../../components/Typo/Typo";

const Reminder = () => {
    const [tab, setTab] = useState(0);

    return <PostIt item>
        <Grid item><Typo variant={'h6'}>Reminder :</Typo></Grid>
        blah blah
    </PostIt>
}

export default Reminder;