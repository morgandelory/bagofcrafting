import {Grid, makeStyles} from "@material-ui/core";
import {useEffect, useState} from "react";
import Pedestals from "../../../data/pedestals.json"
import Collectible from "../../../components/Collectible/Collectible";
import {Scrollbars} from 'react-custom-scrollbars';
import AutoSizer from 'react-virtualized-auto-sizer';

const useStyle = makeStyles({
    item: {
        cursor: 'pointer',
        '& > :hover': {
            position: 'relative',
            top: '-5px',
            filter: 'drop-shadow(5px 5px 1px #222)',
        },
    },
});

const Index = props => {
    const [selected, setSelected] = useState(null);
    const classes = useStyle();

    useEffect(() => props.onChange(selected), [selected])

    return <AutoSizer>
        {({height, width}) => {
            console.log(width, height)
            return <Scrollbars style={{height: height, width: width}}>
                <Grid container direction={'row'} justify={'center'} alignItems={'center'}>
                    {
                        Object.values(Pedestals).reverse().map((item, i) =>
                            <Grid item key={i} className={classes.item} onClick={() => setSelected(item.id)}>
                                <Collectible id={item.id}/>
                            </Grid>)
                    }
                </Grid>
            </Scrollbars>
        }}
    </AutoSizer>
}

export default Index;