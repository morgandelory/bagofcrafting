import {Grid} from "@material-ui/core";
import React from "react";
import {Route, Switch} from "react-router-dom"
import BackgroundImage from "./home.png"
import BagOfCrafting from "./pages/bagofcrafting/BagOfCrafting";
import Encyclopedia from "./pages/encyclopedia/Encyclopedia";
import Menu from "./Menu";

function App() {
    return <div style={{
        height: '100vh',
        width: '100vw',
        background: `no-repeat center/1500px 1180px url(${BackgroundImage})`
    }}>
        <Grid container direction={'column'} alignItems={'center'} style={{height: '92vh'}}>
            <Grid item container style={{height: '20%'}} direction={'column'} justify={'center'}>
                <Grid item xs={4}>
                    <Menu/>
                </Grid>
            </Grid>
            <Grid item container xs>
                <Switch>
                    <Route exact path="/" component={BagOfCrafting}/>
                    <Route exact path="/items" component={Encyclopedia}/>
                </Switch>
            </Grid>
        </Grid>
    </div>
}

export default App;
