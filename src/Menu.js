import {useHistory} from "react-router-dom";
import {Grid, withStyles} from "@material-ui/core";
import React from "react";
import imageMenuItem from "./menu-item.png";
import {Typo} from "./components/Typo/Typo";

const item = {
    root: {
        padding: '32px',
        imageRendering: 'pixelated',
        background: `url('${imageMenuItem}')`,
        backgroundSize: '100% 100%',
    }
}

const MenuItem = withStyles(item)(Grid);

const Menu = () => {
    const history = useHistory();

    const nav = url => history.push(url);

    return <MenuItem container justify={'space-evenly'}>
        <Grid item onClick={() => nav('/')}>
            <Typo variant={'h6'} style={{cursor: 'pointer'}}>Bag of Crafting</Typo>
        </Grid>
        <Grid item onClick={() => nav('/items')}>
            <Typo variant={'h6'} style={{cursor: 'pointer'}}>Items</Typo>
        </Grid>
    </MenuItem>
}

export default Menu;