import {Grid, withStyles} from "@material-ui/core";
import PostItTop from './images/postit-top.png'
import PostItContent from './images/postit-content.png'
import PostItBottom from './images/postit-bottom.png'

const top_style = {
    root: {
        height: '31px',
        imageRendering: 'pixelated',
        background: `url('${PostItTop}')`,
        backgroundSize: '100% 100%'
    }
}

const bottom_style = {
    root: {
        height: '38px',
        imageRendering: 'pixelated',
        background: `url('${PostItBottom}')`,
        backgroundSize: '100% 100%'
    }
}

const content_style = {
    root: {
        paddingLeft: '32px',
        paddingRight: '32px',
        imageRendering: 'pixelated',
        background: `url('${PostItContent}')`,
        backgroundSize: '100% 100%'
    }
}

const Top = withStyles(top_style)(Grid)
const Content = withStyles(content_style)(Grid)
const Bottom = withStyles(bottom_style)(Grid)

const PostIt = props => {
    return <Grid {...props} container alignContent={'flex-start'}>
        <Top item xs={11}/>
        <Content item xs={11}>{props.children}</Content>
        <Bottom item xs={11}/>
    </Grid>
}

export default PostIt;