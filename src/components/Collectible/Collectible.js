import {Grid} from "@material-ui/core";
import SpriteSheet from './images/collectibles.png';
import './collectibles.css';

function Collectible(props) {
    return <Grid container justify={'center'}>
        <div style={{
            imageRendering: 'pixelated' ,
            margin: '2px',
            backgroundRepeat: 'no-repeat',
            backgroundImage: `url(${SpriteSheet})`
        }} className={'sprite collectibles_' + props.id}/>
    </Grid>;
}

export default Collectible;
