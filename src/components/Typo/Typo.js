import {Typography, withStyles} from "@material-ui/core";
import UpHeaval from "../../upheaval.ttf";

const TypoStyle = theme => ({
    root: {
        fontFamily: 'UpHeaval',
        src: `url(${UpHeaval})`
    },
});
export const Typo = withStyles(TypoStyle)(Typography);

const ClickableTypoStyle = theme => ({
    root: {
        cursor: 'pointer',
        opacity: '0.7',
        '&:hover': {
            opacity: '1.0'
        }
    },
});
export const ClickableTypo = withStyles(ClickableTypoStyle)(Typo);